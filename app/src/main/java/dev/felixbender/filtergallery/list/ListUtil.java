package dev.felixbender.filtergallery.list;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Felix Bender
 * @version 0.0.1
 */
public class ListUtil {

    public static List<ListElement> getAllImages(Context context) {
        List<ListElement> elements = new ArrayList<>();

        final String[] cols = {
                MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED
        };

        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                cols,
                null,
                null,
                MediaStore.Images.Media.DATE_ADDED + " DESC");

        List<String> paths = new ArrayList<>();
        if (cursor.moveToFirst()) {
            final int imagePathCol = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                paths.add(cursor.getString(imagePathCol));
            } while (cursor.moveToNext());
        }

        for (String s : paths) {
            ListElement le = new ListElement();
            le.setImage(BitmapFactory.decodeFile(s));
            le.setImageTitle(new File(s).getName());
            elements.add(le);
        }

        return elements;
    }
}
