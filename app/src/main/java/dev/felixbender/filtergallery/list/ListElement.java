package dev.felixbender.filtergallery.list;

import android.graphics.Bitmap;

/**
 * @author Felix Bender
 * @version 0.0.1
 */
public class ListElement {

    private String imageTitle;
    private Bitmap image;

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
